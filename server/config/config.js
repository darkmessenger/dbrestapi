//IMPORT
//class

//object

//function
const express = require('express');
const cors = require('cors');
let config = require('../res/config/config.json');
const dbdrivers = require('../res/config/dbdrivers.json');
const tables = require('../res/config/tables.json');

//function
const printLog = (log) => {
	let dt = new Date();
	console.log("DEBUG -> Config." + dt.toLocaleDateString() + " " + dt.toLocaleTimeString() + "." + log);
}

const getEnv = () => {
	let env = process.env.NODE_ENV;
	printLog(`process.env.NODE_ENV : ${env}`);
//	env = (env != "" && config[env] && env) || "development";
	return env || "development";
}

//BODY
const env = getEnv();
config = config[env];
config.env = env;

const configure = (app) => {
	printLog(`configure[${config.env}]`);
	console.log(config);
	const env = config.env;
	config.maxRequestSize *= 1024 * 1024; //convert to MB

	let drivers = {};
	for (const i in dbdrivers) {
		const driver = dbdrivers[i];
		drivers[driver.fileName] = driver[env];
	}
	config.dbdrivers = drivers;
	config.tables = tables[env];

	app.use(express.raw({ limit: config.maxRequestSize }));
	app.use(express.urlencoded({ limit: config.maxRequestSize, extended: true }));
	app.use(express.json({ limit: config.maxRequestSize }));
	app.use(cors({ origin: true }));
	console.log(config);
}
config.configure = configure;

module.exports = config;
