//IMPORT
//class

//object
const config = require('./config/config');

//function
const express = require('express');

//BODY
//constant
const MAX_SIZE = 100 * 1024 * 1024;

//variables
const app = express();

//function
const printLog = (log) => {
	let dt = new Date();
	console.log("DEBUG -> index." + dt.toDateString() + " " + dt.toTimeString() + "." + log);
}

config.configure(app);
console.log(config);

const mod = require('./module/module.js');

//Router
app.get('/', (req, resp, next) => {
	resp.status(200).json({"status": "Ok"});
});

mod.routers.map(router => {
	app.use('/', router);
});

app.listen(config.port, () => {
	printLog(`listening on port ${config.port}`);
});
