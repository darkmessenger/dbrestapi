const type = {
	INT: "INT",
	FLOAT: "FLOAT",
	STRING: "STRING",
	TIMESTAMP: "TIMESTAMP"
}

const printLog = (log) => {
	let dt = new Date();
	console.log("DEBUG -> IDB." + dt.toDateString() + " " + dt.toTimeString() + "." + log);
}

/**
function DbColumn() {
	this.type = type.STRING;
	this.primary = false;
	this.auto = false;
	this.fromDB = function(val) { return val; }
	this.toDB = function(val) { return val; }
}
*/

const ColumnDescList = [];
ColumnDescList[type.INT] = function() {
	this.fromDB = (val) => { return parseInt(val) || 0; }
	this.toDB = (val) => { return `'${parseInt(val)}'` || 0; }
}
ColumnDescList[type.FLOAT] = function() {
	this.fromDB = (val) => { return parseFloat(val) || 0.0; }
	this.toDB = (val) => { return parseFloat(val) || 0.0; }
}
ColumnDescList[type.STRING] = function() {
	this.fromDB = (val) => { return val || ""; }
	this.toDB = (val) => { return `'${val}'` || "''"; }
}
ColumnDescList[type.TIMESTAMP] = function() {
	this.fromDB = (val) => { return (val || new Date()).getTime(); }
	/**
	 * val[string] : yyyy-MM-dd HH:mm:ss
	 */
	this.toDB = (val) => { 
		const dt = new Date(Date.parse(val)) || new Date();
		return "'" + dt.getFullYear() + "-" + 
			(dt.getMonth() + 1) + "-" +
			dt.getDate() + " " +
			dt.getHours() + ":" +
			dt.getMinutes() + ":" +
			dt.getSeconds() + "." +
			dt.getMilliseconds() + "'";
	}
//	this.fromDB = (val) => { 
//		let stamp = (parseInt(val) * 1000.0) || 0;
//		return new Date(stamp);
//	}
//
//	this.toDB = (val) => { return (Date.parse(val) / 1000.0) || 0; }
}

/**
 * default column description list
 */
const createColumnDescriptions = () => {
	const descList = [];
	for (const key in type) {
		const index = type[key];
		descList[index] = new ColumnDescList[index];
	}
	return descList;
}

module.exports = {
	util: {
		createColumnDescriptions
	},
	interface: {
		type: type,
		desc: async (table) => { 
			throw new Error("//TODO #implement get table description");
		},
		create: async (table, record) => { 
			throw new Error("//TODO #implement create record");
		},
		read: async (table, criteria, values) => { 
			throw new Error("//TODO #implement read one record");
		},
		update: async (table, record, criteria, values) => {
			throw new Error("//TODO #implement update one record");
		},
		delete: async (table, criteria, values) => {
			throw new Error("//TODO #implement delete one record");
		}
	}
}
