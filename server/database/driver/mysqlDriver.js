/**
 * module to implement specific DB api
 * also the abstraction of Column description and CRUD operation
 */
const mariadb = require('mariadb');

let dbUtil = null;
let pool = null;
let IDB = null; //interface

const printLog = (log) => {
	let dt = new Date();
	console.log("DEBUG -> mysqlDriver." + dt.toDateString() + " " + dt.toTimeString() + "." + log);
}

/**
 * list of IDB.DbColumn
 * contain the default column description and a parser input output method for each specific column type
 */

/**
 * implement IDB.DbColumn object based on database specification
 */
const createColumn = (item) => {
	const type = getColumnType(item);
	const descList = dbUtil.createColumnDescriptions();
	//TODO ### add custom column desc based on DB that doesn't exists on IDB
	const desc = descList[type] || descList[IDB.type.STRING];
	return {
		name: item.Field,
		type: type,
		isPrimary: isPrimary(item),
		auto: isAuto(item),
		fromDB: desc.fromDB,
		toDB: desc.toDB
	};
}

const query = async (sql) => {
	printLog(`query.SQL : ${sql}`);

	let cn = null;
	return pool.getConnection()
	.then(conn => {
		cn = conn;
		return conn.query(sql);
	}).catch(e => {
		printLog(`query failed : ${e.message}`);
		throw e;
	}).finally(() => {
		printLog(`close connection : ${cn}`);
		cn && cn.end();
	});
}

/**
 * getColumnType based on Type on DB
 * columnDesc : Description object of the column
 */
const getColumnType = (columnDesc) => {
	const mysqlType = columnDesc.Type;
	if (mysqlType.toLowerCase().indexOf("varchar") >= 0) {
		return IDB.type.STRING;
	} else if (mysqlType.toLowerCase().indexOf("int") >= 0) {
		return IDB.type.INT;
	} else if (
		mysqlType.toLowerCase().indexOf("float") >= 0 ||
		mysqlType.toLowerCase().indexOf("decimal") >= 0
	) {
		return IDB.type.FLOAT;
	} else if (mysqlType.toLowerCase().indexOf("timestamp") >= 0) {
		return IDB.type.TIMESTAMP;
	}

	return IDB.type.STRING;
}

/**
 * determine if this column is primary key
 */
const isPrimary = (columnDesc) => {
	return columnDesc.Key.toLowerCase().indexOf("pri") >= 0;
}

/**
 * determine if this column is auto increment
 */
const isAuto = (columnDesc) => {
	return columnDesc.Extra.toLowerCase().indexOf("auto_increment") >= 0;
}

/**
 * create table description
 * table : table name
 */
const desc = async (table) => {
	printLog(`desc : ${table}`);
	return query(`DESC ${table}`)
	.then(results => {
		const columns = {};
		printLog("size : " + results.length);
		results.map(item => {
			printLog(`desc : ${JSON.stringify(item)}`);
			const column = createColumn(item);
			printLog(`column : ${JSON.stringify(column)}`);
			columns[item.Field] = column;
		});

		return columns;
	});
}

/**
 * CREATE
 * table : table specification for this record
 * record : record to be created in DB
 */
const create = async (table, record) => {
	printLog(`create : ${JSON.stringify(record)}`);

	const columns = [];
	const values = [];
	for (const col in table.columns) {
		const column = table.columns[col];
		if (column.auto) continue; //skip auto_increment
		columns.push(col);
		const val = record[col];
		values.push(column.toDB(val));
	}

	const sql = `INSERT INTO ${table.name} (${columns.join(", ")}) VALUES (${values.join(", ")})`;
	return query(sql)
	.then(result => {
		printLog(`create success : ${result}`);
		return {
			affectedRows: result.affectedRows,
			insertId: parseInt(result.insertId),
			warningStatus: result.warningstatus
		}
	});
}

/**
 * READ based on specific criteria
 * table : table specification for this record
 * criteria
 * - WHERE (id = ${values.id} AND name LIKE (${values.name})) OR height >= ${values.height}
 * values : contain criteria values
 */
const read = async (table, criteria, values) => {
	printLog(`read[${criteria}] : ${JSON.stringify(values)}`);

	if (!table) throw new Error("no configuration related to this model!!!");

	//collect all column and parse all value based on column description
	const columns = [];
	for (const col in table.columns) {
		const column = table.columns[col];
		columns.push(col);

		//criteria values
		const val = values && values[col];
		if (!val) continue;
		values[col] = column.toDB(val);
	}

//	const filter = criteria && values && (criteria && " " + eval("`" + criteria + "`;")) || "";
	let filter = null;
	if (criteria == null) {
		filter = "";
	} else if (values == null) {
		filter = ` ${criteria}`;
	} else {
		filter = " " + eval("`" + criteria + "`;");
	}
	const sql = `SELECT ${columns.join(", ")} FROM ${table.name}${filter}`;
	return query(sql)
	.then(results => {
		printLog(`read.success : ${(results && results.length)}`);
		//parse
		for (let row = 0; row < results.length; row ++) {
			const record = results[row];
			for (const col in record) {
				const column = table.columns[col];
				const val = record[col];
				record[col] = column.fromDB(val);
				printLog(`parse[${col}] : ${val} -> ${record[col]}`);
			}
		}
		return results;
	});
}

/**
 * UPDATE based on specific criteria
 * table : table specification for this record
 * record : contain updated values
 * criteria
 * WHERE
 * values : contain criteria values
 */
const update = async (table, record, criteria, values) => {
	printLog(`update : ${JSON.stringify(values)} => ${JSON.stringify(record)}`);
	
	const columns = [];
//	const sql = `UPDATE ${table.name}
	const newValues = [];
	if (!table) throw new Error("no configuration related to this model!!!");
	if (!record) throw new Error("no values to be updated!!!");

	for (const col in table.columns) {
		const column = table.columns[col];

		//values to be updated
		const newVal = record && record[col];
		if (!column.auto && newVal !== undefined) {
//			printLog("col[" + col + "] : " + newVal + ", " + (newVal === undefined));
			newValues.push(col + " = " + column.toDB(newVal));
		}

		//criteria values
		const val = values && values[col];
		if (!val) continue;
		values[col] = column.toDB(val);
	}
	printLog(`newValues : ${JSON.stringify(newValues)}`);
	if (newValues.length <= 0) throw new Error("no values to be updated!!!");

	const filter = criteria && values && (criteria && " " + eval("`" + criteria + "`;")) || "";
	const sql = `UPDATE ${table.name} SET ${newValues.join(", ")}${filter}`;
	return query(sql)
	.then(result => {
		printLog(`update success : ${result}`);
		return {
			affectedRows: result.affectedRows,
			insertId: parseInt(result.insertId),
			warningStatus: result.warningstatus
		}
	});
}

/**
 * DELETE based on specific criteria
 * table : table specification for this record
 * criteria
 * - WHERE (id = ${values.id} AND name LIKE (${values.name})) OR height >= ${values.height}
 * values : contain criteria values
 */
const deleteRecord = async (table, criteria, values) => {
	printLog(`delete : ${JSON.stringify(values)}`);

	if (!table) throw new Error("no configuration related to this model!!!");

	//collect all column and parse all value based on column description
	for (const col in table.columns) {
		const column = table.columns[col];

		//criteria values
		const val = values && values[col];
		if (!val) continue;
		values[col] = column.toDB(val);
	}

	const filter = criteria && values && (criteria && " " + eval("`" + criteria + "`;")) || "";
	const sql = `DELETE FROM ${table.name}${filter}`;
	return query(sql)
	.then(result => {
		printLog(`delete success : ${result}`);
		return {
			affectedRows: result.affectedRows,
			insertId: parseInt(result.insertId),
			warningStatus: result.warningstatus
		}
	});
}

module.exports = ({
	util,
	interface,
	config
}) => {
	dbUtil = util;
	IDB = interface;
	printLog(`create mysqlDriver [pool : ${pool}]`);
	pool = mariadb.createPool(config);
	console.log(config);
	return Object.assign(IDB, {
		query,
		desc,
		create,
		read,
		update,
		delete: deleteRecord
	});
}
