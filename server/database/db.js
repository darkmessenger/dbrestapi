/**
 * Database Wrapper
 */

const config = require('../config/config.js');
const IDB = require('./IDB.js');
const drivers = {};

const printLog = (log) => {
	let dt = new Date();
	console.log("DEBUG -> db." + dt.toDateString() + " " + dt.toTimeString() + "." + log);
}

module.exports = (driverName) => {
	let driver = drivers[driverName];
	printLog(`db[${driverName}].exists : ${driver != null}`);
	if (!driver) {
		const dbConfig = config.dbdrivers[driverName];
		driver = require(`./driver/${driverName}`)({
			util: IDB.util, 
			interface: IDB.interface, 
			config: dbConfig
		});
		printLog(`load new driver for '${driverName}'`);
		console.log(driver);
		drivers[driverName] = driver;
	}

	return driver;
}
