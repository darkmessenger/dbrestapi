//IMPORT

//BODY
const printLog = (log) => {
	let dt = new Date();
	console.log("DEBUG -> table." + dt.toDateString() + " " + dt.toTimeString() + "." + log);
}

function Table({
	driverName, 
	name
}) {
	const db = require('./db.js')(driverName);

	printLog(`create Table(${name}) using ${driverName}`);

	const create = async (record) => {
		return initialize().then(() => { return db.create(table, record); });
	}

	const read = async (criteria, values) => {
		return initialize().then(() => { return db.read(table, criteria, values); });
	}

	const update = async (record, criteria, values) => {
		return initialize().then(() => { return db.update(table, record, criteria, values); });
	}

	const deleteRecord = async (criteria, values) => {
		return initialize().then(() => { return db.delete(table, criteria, values); });
	}

	const list = async () => {
		return read(null, null);
	}

	const generateCriteria = function(values) {
		let criteria = "";
//		for (const col in this.columns) {
//			const column = this.columns[col];
//			if (!column.isPrimary) continue;
//			criteria += (criteria == "" ? "WHERE " : " AND ") +
//				`${column.name} = ` + "${values.id}";
//		}
		for(const col in values) {
			criteria += (criteria == "" ? "WHERE " : " AND ") +
				`${col} = ` + "${values." + col + "}";
		}
		return criteria;
	}

	const table = Object.assign(this, {
		name: name,
		columns: null,
		create,
		read,
		update,
		delete: deleteRecord,
		list,
		generateCriteria
	});

	const initializer = 
		new Promise((resolve, reject) => {
			printLog(`initialize[exists : ${table.columns != null}]`);
			if (table.columns != null) {
				resolve(table);
				return;
			}

			printLog("initializing...");
//			setTimeout(() => {
				db.desc(name)
				.then(columns => {
					table.columns = columns;
					printLog(`initialize '${name}' success...`);
					printLog(`table.columns : ${JSON.stringify(table.columns)}`);
					resolve(table);
				}).catch(e => {
					printLog(`initialize '${name}' failed : ${e.message}`);
					resolve(table);
				});
//			}, 5000);
		});

	const initialize = async () => {
		return initializer;
	}

	table.initialize = initialize;
}

Table.create = (config) => { 
	const table = new Table(config);
	console.log(table);
	table.initialize()
	.then(modified => {
		printLog(`initialized : `);
		console.log(table);
		return table;
	}).catch(e => {
		printLog(`initialization failed : ${e.message}`);
		return e;
	});
	return table;
}

module.exports = Table;
