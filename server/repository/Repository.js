const Table = require('../database/Table.js');

const printLog = (log) => {
	let dt = new Date();
	console.log("DEBUG -> Repository.js." + dt.toString() + "." + log);
}

function Repository({ 
	driverName, //database configuration based on driver name on config/drivers.json
	tableName, //name of the table
	baseUrl, //url
	exposed //array list of exposed function to api
}) {

	const table = Table.create({
		driverName, 
		name: tableName
	});
	this.table = table;

	const create = async (record) => {
		printLog(`create : ${JSON.stringify(record)}`);
		return table.create(record)
		.then(result => {
			printLog(`create success : ${JSON.stringify(result)}`);
			return result;
		}).catch(e => {
			printLog(`create failed : ${e.message}`);
			throw e;
		});
	}
	this.create = create;

	const read = async (id) => {
		printLog(`read[${id}]`);
		const values = { id: id }
		return table.read(
			table.generateCriteria(values), values
		).then(records => {
			printLog("read success");
			return records;
		}).catch(e => {
			printLog("read failed : " + e.message);
			throw e;
		});
	}
	this.read = read;

	const update = async (id, record) => {
		printLog(`update[${id}] : ${JSON.stringify(record)}`);
		const values = { id: id }
		return table.update(
			record,
			table.generateCriteria(values), 
			values
		).then(result => {
			printLog(`update success : ${JSON.stringify(result)}`);
			return result;
		}).catch(e => {
			printLog(`update failed : ${e.message}`);
			throw e;
		});
	}
	this.update = update;

	const deleteRecord = async (id) => {
		printLog(`delete[${id}]`);
		const values = { id: id }
		return table.delete(
			table.generateCriteria(values), 
			values
		).then(result => {
			printLog(`delete success : ${JSON.stringify(result)}`);
			return result;
		}).catch(e => {
			printLog(`delete failed : ${e.message}`);
			throw e;
		});
	}
	this.delete = deleteRecord;

	//list of question
	const list = () => {
		printLog("list");
		return table.list()
		.then(records => {
			printLog("list success");
			return records;
		}).catch(e => {
			printLog("list failed : " + e.message);
			throw e;
		});
	}
	this.list = list;
}

Repository.create = (config) => { return new Repository(config); }

module.exports = Repository;
