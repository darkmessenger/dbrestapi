const express = require('express');
const Repository = require('../repository/Repository.js');

const printLog = (log) => {
	let dt = new Date();
	console.log("DEBUG -> ApiController." + dt.toLocaleDateString() + " " + dt.toLocaleTimeString() + "." + log);
}

function ApiController(config) {
	let { 
		baseUrl, //url
		exposed //array list of exposed function to api
	} = config;
	const router = express.Router();
	this.router = router;

	const repository = Repository.create(config);

	baseUrl = baseUrl || "";
	exposed = exposed || { create: '', read: '', update: '', delete: '', list: ''};

	const create = (req, resp, next) => {
		const record = req.body;
		printLog(`create : ${JSON.stringify(record)}`);
		repository.create(record)
		.then(result => {
			printLog(`create success : ${JSON.stringify(result)}`);
			resp.status(200).json(result);
			return result;
		}).catch(e => {
			printLog(`create failed : ${e.message}`);
			resp.status(500).json({ message: "Something went wrong !!!" });
			return e;
		});
	}
	this.create = create;

	const read = (req, resp, next) => {
		const id = req.params.id;
		printLog(`read[${id}]`);
		repository.read(id)
		.then(records => {
			if (!records || records.length <= 0) {
				resp.status(404).json({ message: "Record not found !!!" });
				return records;
			}
			printLog("read success");
			resp.status(200).json(records[0]);
			return records;
		}).catch(e => {
			printLog("read failed : " + e.message);
			resp.status(500).json({ message: "Something went wrong !!!" });
			return e;
		});
	}
	this.read = read;

	const update = (req, resp, next) => {
		const id = req.params.id;
		const record = req.body;
		printLog(`update[${id}] : ${JSON.stringify(record)}`);
		repository.update(
			id,
			record
		).then(result => {
			printLog(`update success : ${JSON.stringify(result)}`);
			resp.status(200).json(result);
			return result;
		}).catch(e => {
			printLog(`update failed : ${e.message}`);
			resp.status(500).json({ message: "Something went wrong !!!"});
		});
	}
	this.update = update;

	const deleteRecord = (req, resp, next) => {
		const id = req.params.id;
		printLog(`delete[${id}]`);
		repository.delete(id)
		.then(result => {
			printLog(`delete success : ${JSON.stringify(result)}`);
			resp.status(200).json(result);
			return result;
		}).catch(e => {
			printLog(`delete failed : ${e.message}`);
			resp.status(500).json({ message: "Something went wrong !!!"});
		});
	}
	this.delete = deleteRecord;

	//list of question
	const list = (req, resp, next) => {
		printLog("list");
		repository.list()
		.then(records => {
			printLog("list success");
			resp.status(200).json(records);
			return records;
		}).catch(e => {
			printLog("list failed : " + e.message);
			resp.status(500).json({ message: "Something went wrong !!!" });
		});
	}
	this.list = list;

	//expose default CRUD operation as API based on configuration

	//create route only if method is not null (set to null to hide it from api)
	exposed.create != null && router.post(`${baseUrl}${exposed.create}`, create);
	exposed.read != null && router.get(`${baseUrl}${exposed.read}/:id`, read);
	exposed.update != null && router.patch(`${baseUrl}${exposed.update}/:id`, update);
	exposed.delete != null && router.delete(`${baseUrl}${exposed.delete}/:id`, deleteRecord);
	exposed.list != null && router.get(`${baseUrl}${exposed.list}`, list);
	printLog(`exposed : ${baseUrl}, ${JSON.stringify(exposed)}`);
}

ApiController.create = (config) => { return new ApiController(config); }

module.exports = ApiController;
