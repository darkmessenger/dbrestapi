//IMPORT
const ApiController = require('../api/ApiController.js');
const config = require('../config/config.js');

const printLog = (log) => {
	let dt = new Date();
	console.log("DEBUG -> Module.js." + dt.toString() + "." + log);
}

printLog(`initialize routers`);
const routers = config.tables.map(table => {
	return ApiController.create({
		driverName: config.driverName,
		tableName: table.table,
		baseUrl: table.baseUrl,
		exposed: table.exposed
	}).router;
});
printLog(`routers.size : ${routers.length}`);

module.exports = {
	routers
}
