create database if not exists deus;

use deus;

create table if not exists questions
(
	id int not null primary key auto_increment,
	question varchar(255) not null,
	templateId int
);
