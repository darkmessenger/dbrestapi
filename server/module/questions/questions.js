//IMPORT
const ApiController = require('../../api/ApiController.js');

//variables
const api = ApiController.create({
	driverName: "mysqlDriver",
	tableName: "questions",
	baseUrl: '',
	exposed: { create: '/', read: '/', update: '/', delete: '/', list: '/'}
});

module.exports = {
	router: api.router
}

const printLog = (log) => {
	let dt = new Date();
	console.log("DEBUG -> questions." + dt.toLocaleDateString() + " " + dt.toLocaleTimeString() + "." + log);
}
